<?php
return [
    \Plumbus\Router::DEFAULT_REQUEST_TYPE_NAME => [
        '' => 'index',
        'component' => [
            'plumbus' => [
                '%s=componentName' => 'component',
            ]
        ]
    ]
];
