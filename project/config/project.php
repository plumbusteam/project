<?php
$config = require_once 'main.php';

$config['social'] = [
    'networks' => [
        \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_VKONTAKTE => [
            'enabled' => true,
            'app_id' => env('VK_APP_ID'),
            'app_secret' => env('VK_APP_SECRET'),
            'connect_url' => 'https://' . env('DOMAIN') . '/connect/vk'
        ],
        \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_FACEBOOK => [
            'enabled' => true,
            'app_id' => env('FB_APP_ID'),
            'app_secret' => env('FB_APP_SECRET'),
            'connect_url' => 'https://' . env('DOMAIN') . '/connect/fb'
        ],
        \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_ODNOKLASSNIKI => [
            'enabled' => true,
            'app_id' => env('OK_APP_ID'),
            'app_secret' => env('OK_APP_SECRET'),
            'app_public' => env('OK_APP_PUBLIC'),
            'connect_url' => 'https://' . env('DOMAIN') . '/connect/ok',
        ],
    ]
];

return $config;