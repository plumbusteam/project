<?php

$modules['text_on_main'] = [
    'class' => \PlumbusProject\Modules\HTML\Text::class,
    'template' => 'html/main.twig',
    'action' => 'showTextOnMain',
    'cache' => 0,
    'css' => [
        '/static/css/module/html/text/main.css'
    ]
];

$modules['component'] = [
    'class' => \PlumbusProject\Modules\HTML\Text::class,
    'template' => 'html/component.twig',
    'action' => 'showComponentPage',
    'cache' => 0,
    'css' => [
        '/static/css/module/html/text/component.css'
    ]
];

return $modules;