<?php
$modules = require_once 'modules.php';

$pages = [
    'index' => new \Plumbus\Core\Controller\Page\SimplePage([
        'layout' => 'index',
        'cache' => 10,
        'css' => [
            '/static/css/project.css',
            '/static/css/layouts/index.css'
        ],
        'title' => 'Plumbus project',
        'description' => 'Plumbus project',
        'blocks' => [
            'head' => [],
            'content' => [
                'text_on_main' => $modules['text_on_main']
            ]
        ],
    ]),
    'component' => new \Plumbus\Core\Controller\Page\SimplePage([
        'layout' => 'component',
        'css' => [
            '/static/css/project.css',
            '/static/css/layouts/component.css'
        ],
        'title' => 'Plumbus project component',
        'description' => 'Plumbus project component',
        'blocks' => [
            'head' => [],
            'content' => [
                'component' => $modules['component']
            ]
        ],
    ])
];
return $pages;
