<?php namespace PlumbusProject\Modules\HTML;

use Plumbus\Core\Module\Base;
use Plumbus\Exception\NotFound;

class Text extends Base
{
    public function actionShowTextOnMain()
    {
        return [];
    }

    public function actionShowComponentPage(array $variables = null)
    {
        if (!isset($variables['componentName'])) {
            throw new NotFound('Empty component name');
        }

        $componentName = (string) $variables['componentName'];

        return [
            'componentName' => $componentName,
        ];
    }
}
