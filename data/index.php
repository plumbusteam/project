<?php
header('X-Accel-Expires: 0');
session_cache_limiter('public');
error_reporting(E_ALL);
ini_set('display_errors', true);

/**
 * Подключаем autoloader
 */
require_once __DIR__ . '/../vendor/autoload.php';
/**
 * Создаем приложение с проектной конфигурацией
 */

$application = new \Plumbus\Core\Application\Web(
    __DIR__ . '/../project/'
);
/**
 * Запускаем приложение. Вывод будет выведен после обработки запроса.
 */
$application->run();